#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from dotenv import load_dotenv

from litu import Litu

load_dotenv()

if __name__ == '__main__':
    Litu().run(os.getenv('LITU_TOKEN'))
